__author__ = 'jnonon'
__name__ = 'skinclassifier'

from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_score
from sklearn.metrics import confusion_matrix
from sklearn.externals import joblib
import pandas as pd
import os


def get_path():
    return os.path.dirname(__file__)

def construct_classifier():

    path = get_path() +"/datasets/snk_shuffled.csv"


    df = pd.DataFrame.from_csv(path, index_col=None)
    target = df.pop('C')

    #clf = tree.DecisionTreeClassifier()
    clf = RandomForestClassifier(n_estimators=20, max_depth=None, min_samples_split=2, random_state=10)
    df_train, df_test, target_train, target_test = train_test_split(df, target, test_size=0.33, random_state=42)
    clf.fit(df_train, target_train)
    scores = cross_val_score(clf, df_train, target_train, cv=5)
    print("Accuracy: %0.7f (+/- %0.7f)" % (scores.mean(), scores.std() * 2))
    predicted_target = clf.predict(df_test)
    cm = confusion_matrix(predicted_target, target_test)

    joblib.dump(clf, get_path()+'/compiled/model.pkl')
    print "Classifier saved... Yey!"
    print cm

def get_classifier():

    model_path = get_path()+'/compiled/model.pkl'

    if not os.path.isfile(model_path):
        construct_classifier()

    return joblib.load(model_path)