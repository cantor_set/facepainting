__author__ = 'jnonon'
import os
import model.skinclassifier as skinner
import facefinder
import cv2
import numpy as np
import pyramidblend as blender
import os
import shelve

cache = shelve.open('cache.db')

#alpha makes the overlay area darker or brighter
alpha =  0.05
version = 7
intensity_escale_adjust_percent = 0.05
clf = skinner.get_classifier()
in_image = 'wolf.jpg'
in_overlay = 'blue_overlay.png'
sources = "overlays"

mask_name = 'mask%d.jpg'%version
img = cv2.imread(os.path.join(os.pardir, 'images', 'sources')+'/%s'%in_image, cv2.CV_LOAD_IMAGE_COLOR)
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)/(255*(1+intensity_escale_adjust_percent))
overlay = cv2.imread(os.path.join(os.pardir, 'images', sources)+'/%s'%in_overlay, cv2.CV_LOAD_IMAGE_COLOR)

#blured_image = cv2.medianBlur(img, 3)
#blured_image = cv2.GaussianBlur(img, (5,5), 0.7)
#img = blured_image

if os.path.isfile(mask_name):
    bin_mask = cache['bin_mask']

    overlay_coordinates = cache['overlay_coodinates']
    print "No calculation"
    im_ycrcb = cache['im_ycrcb']
    #0.969879725086
    #print np.mean(bin_mask)
else:
    print "Calculation"

    faces = facefinder.find_faces(img)
    bin_mask, overlay_coordinates = facefinder.create_faces_mask(faces, img, clf)

    print np.mean(bin_mask)
    cache['bin_mask'] = bin_mask
    cache['overlay_coodinates'] = overlay_coordinates
    cv2.imwrite(mask_name, 255*bin_mask)

    im_ycrcb = cv2.cvtColor(img, cv2.COLOR_BGR2YCR_CB)

    skin_ycrcb_mint = np.array((0, 133, 77))
    skin_ycrcb_maxt = np.array((255, 173, 127))
    skin_ycrcb = cv2.inRange(im_ycrcb, skin_ycrcb_mint, skin_ycrcb_maxt)

    cache['im_ycrcb'] = im_ycrcb


mask = bin_mask.astype(float)

#cv2.drawContours(mask, contours, contourIdx,
image_overlay = np.ones_like(img)

# for (y, x, w, h) in faces:
#     resized_image = cv2.resize(overlay, (w, h))
#     image_overlay[x:x+w, y:y+h] = resized_image
#     #image_overlay = image_overlay * mask

for (x_min, x_max, y_min, y_max) in overlay_coordinates:
    resized_image = cv2.resize(overlay, (y_max-y_min, x_max-x_min))
    #minimask = mask[x_min:x_max, y_min:y_max]
    minimask = bin_mask[x_min:x_max, y_min:y_max]
    mini_im_ycrcb = im_ycrcb[x_min:x_max, y_min:y_max]

    #ret, minimask = cv2.threshold(bin_mask, 10, 255, cv2.THRESH_BINARY)

    print "Section Shape", resized_image.shape
    print "Mask Shape", minimask.shape
    print "Super mask Shape", mini_im_ycrcb.shape
    #image_overlay[x_min:x_max, y_min:y_max]= cv2.bitwise_and(resized_image, minimask)

    masked = cv2.add(resized_image, resized_image, mask=minimask.astype(np.uint8))
    #masked = cv2.add(resized_image, resized_image, mask=mini_im_ycrcb.astype(np.uint8))
    #masked = []
    #for channel in range(3):
        #masked.append(cv2.bitwise_and(resized_image[:, :, channel], minimask))
        #masked.append(resized_image[:, :, channel])
        #masked.append(resized_image[:, :, channel] - minimask)
        #miniimage = resized_image[:, :, channel]
        # mini_im_ycrcb[:, :, channel]
        #addition_mask =  cv2.add(miniimage, miniimage, mask=minimask.astype(np.uint8))

     #   masked.append(cv2.bitwise_and(resized_image[:, :, channel], minimask))



        #masked.append(addition_mask)

    #masked = cv2.merge(masked)



    #print masked.shape
    intensity_escaler = img_gray[x_min:x_max, y_min:y_max]
    image_overlay[x_min:x_max, y_min:y_max] = masked.astype(np.float) #*alpha

    for channel in range(3):
        #image_overlay[x_min:x_max, y_min:y_max, channel] = mini_im_ycrcb[:, :, channel]*intensity_escaler

        #newMat = mini_im_ycrcb[:, :, channel] + image_overlay[x_min:x_max, y_min:y_max, channel]
        #newMat = mini_im_ycrcb[:, :, channel]
        newMat = image_overlay[x_min:x_max, y_min:y_max, channel]
        image_overlay[x_min:x_max, y_min:y_max, channel] = newMat*intensity_escaler

        #image_overlay[x_min:x_max, y_min:y_max, channel] = image_overlay[x_min:x_max, y_min:y_max, channel]*intensity_escaler


cv2.imwrite('image_overlay_%d.jpg'%version, image_overlay)

out_layers = []

image_overlay = image_overlay.astype(float)
#img = img.astype(float)
#
for channel in range(3):
     outimg = blender.run_blend(img[:, :, channel], image_overlay[:, :, channel], mask[:, :])
     out_layers.append(outimg)
outimg = cv2.merge(out_layers)

beta = 0.9
#outimg = cv2.addWeighted(img, beta, image_overlay, 1-beta, 0.0)
#blured_image = cv2.GaussianBlur(outimg, (5,5), 0.9)

cv2.imwrite("desaturate_colorized%d.jpg"%version, outimg)
#cv2.imwrite('mask.jpg', mask)