__author__ = 'jnonon'
import cv2
import numpy as np
import pyramidblend as blender
import os
import shelve
import facefinder

cache = shelve.open('cache.db')

source_image_path = "wolf.jpg"
source_mask_path = "mask8.jpg"
source_overlay_path = "overlay17.jpg"
version = 1


#source Image
img = cv2.imread(source_image_path, cv2.CV_LOAD_IMAGE_COLOR)
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# faces = facefinder.find_faces(img)
# bin_mask, overlay_coordinates = facefinder.create_faces_mask(faces, img, clf)
#
# print np.mean(bin_mask)
# cache['bin_mask'] = bin_mask
# cache['overlay_coodinates'] = overlay_coordinates
# cv2.imwrite(mask_name, 255*bin_mask)
#
#
# print img_gray.shape

#mask
mask = cv2.imread(source_mask_path)

mask = mask.astype(float)/255.0

#overlay
overlay = cv2.imread(source_overlay_path, cv2.CV_LOAD_IMAGE_COLOR)

out_layers = []

image_overlay = overlay.astype(float)

for channel in range(3):
     scaled = image_overlay[:, :, channel]*img_gray[:, :]/255.0
     outimg = blender.run_blend( img[:, :, channel], scaled, mask[:, :, channel])
     #outimg = blender.run_blend(image_overlay[:, :, channel], img[:, :, channel], mask[:, :, channel])
     out_layers.append(outimg)

outimg = cv2.merge(out_layers)
cv2.imwrite("out%d.jpg"%version, outimg)