__author__ = 'jnonon'
import numpy as np
import cv2
import os


def find_faces(img, pixels_offset=10):

    # face_cascade = cv2.CascadeClassifier(os.path.join(os.pardir, 'extras', 'haarcascade')+'/haarcascade_frontalface_default.xml')
    # face_cascade1 = cv2.CascadeClassifier(os.path.join(os.pardir, 'extras', 'haarcascade')+'/haarcascade_frontalface_alt.xml')
    # face_cascade2 = cv2.CascadeClassifier(os.path.join(os.pardir, 'extras', 'haarcascade')+'/haarcascade_frontalface_alt2.xml')
    face_cascade3 = cv2.CascadeClassifier(os.path.join(os.pardir, 'extras', 'haarcascade')+'/haarcascade_frontalface_alt_tree.xml')
    #img = cv2.imread(os.path.join(os.pardir, 'images', 'sources')+'/black.jpg')
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade3.detectMultiScale(gray, 1.2, 5)



    faces_with_offset = []
    for (y, x, w, h) in faces:
        print (y, x, w, h)
        x = x - pixels_offset
        y = y - pixels_offset

        w = int(1.5*w) + 2*pixels_offset
        h = h + 2*pixels_offset
        print (y, x, w, h)

        faces_with_offset.append((y, x, w, h))

    return faces_with_offset

def create_faces_mask(faces, img, clf):

    max_x = img.shape[0]
    max_y = img.shape[1]

    mask = np.zeros((max_x, max_y))

    overlays = []

    for (y, x, w, h) in faces:

        i_min = max_x
        i_max = 0

        j_min = max_y
        j_max = 0
        #print (y, x, w, h)
        #TODO: May fail if pixel offset is less than minimum or maximum
        for i in range(x, x + w):
            for j in range(y, y + h):

                value = clf.predict(img[i,j])

                #mask[i,j] = value
                mask[i,j] = 0 if value == 1 else 1

                if value == 0:
                    i_min = min(i_min, i)
                    j_min = min(j_min, j)

                    i_max = max(i_max, i)
                    j_max = max(j_max, j)

        overlays.append((i_min, i_max, j_min, j_max))


    return mask, overlays


