# README #

This project consist on a pipeline of four steps: Recognize the position of the faces in a given image; from the candidate spaces, detect which pixels to can or should be colorized; from the detected pixels, create a blending mask; create an overlay image with a particular color or pattern to be blended with the original image; finally colorize the area corresponding with another image using pyramid blending techniques.

### Requeriments ###